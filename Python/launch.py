from sofatools import Launcher
import os

curDir = os.getcwd()

fmm = Launcher({
    "SOFA_PATH" : "path_to_sofa_build/bin/runSofa",
    "SCENE_PATH" : os.path.join(curDir, "scene_FMM.py"),
    "MESH" : os.path.join(curDir, "cube.vtk"),
    "RESULT_DIR_PATH" : os.path.join(curDir, "Output"),
    "INIT_NODES" : [0],
    "EXPORT_AT_EACH_STEP" : 0
})

fmm["VTK_FILENAME"] = os.path.join(fmm["RESULT_DIR_PATH"], "fmm.vtk")

fmm.run()
