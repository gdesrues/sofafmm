import Sofa

class SofaFMM(Sofa.PythonScriptController):
    def __init__(self, node):
        self.createGraph(node)
        return None

    def createGraph(self, rootNode):
        self.rootNode = rootNode
        rootNode.createObject('RequiredPlugin', pluginName='FMM')
        rootNode.createObject('MeshVTKLoader', name='loader', filename="cube.vtk")
        rootNode.createObject('Mesh', name="mesh", src="@loader")
        rootNode.createObject("ZoneFMM",
                filename = "fmm.vtk",
                exportAtEachStep = 0,
                initNodes = "0",
                position="@mesh.position"
        )
        return 0

def createScene(rootNode):
    myScene = SofaFMM(rootNode)
    return 0
