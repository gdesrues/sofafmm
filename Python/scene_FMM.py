import Sofa
import argparse
import json


class SofaFMM(Sofa.PythonScriptController):
    def __init__(self, node, args):
        global params
        params = json.load(open(args[0],'r'))
        self.createGraph(node)
        return None


    def createGraph(self, rootNode):
        self.rootNode = rootNode

        # Please download the FMM plugin for sofa : https://gitlab.inria.fr/gdesrues1/sofafmm
        # For more information, read https://www.sofa-framework.org/community/doc/using-sofa/build-a-plugin/
        rootNode.createObject('RequiredPlugin', pluginName='FMM')

        rootNode.createObject('MeshVTKLoader', name='loader', filename=params["MESH"])
        rootNode.createObject('Mesh', name="mesh", src="@loader")

        rootNode.createObject("ZoneFMM",
                filename = params["VTK_FILENAME"],
                exportAtEachStep=params["EXPORT_AT_EACH_STEP"],
                initNodes = params["INIT_NODES"],
                position="@mesh.position"
        )

        return 0



def createScene(rootNode):
    parser = argparse.ArgumentParser()
    parser.add_argument('--argv', action='store_true')
    parsed, args = parser.parse_known_args()
    myScene = SofaFMM(rootNode, args)
    return 0
