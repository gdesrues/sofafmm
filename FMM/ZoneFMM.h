/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 beta 4      *
*                (c) 2006-2009 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_ZoneFMM_H
#define SOFA_ZoneFMM_H

#include <sofa/core/DataEngine.h>
//#include <sofa/core/behavior/OdeSolver.h>

#include <SofaBaseTopology/TetrahedronSetTopologyContainer.h>
#include <SofaBaseTopology/TetrahedronSetGeometryAlgorithms.h>
#include <SofaBaseTopology/PointSetGeometryAlgorithms.h>
#include <SofaBaseTopology/PointSetTopologyAlgorithms.h>
#include <SofaBaseTopology/PointSetTopologyContainer.h>
#include <sofa/core/topology/BaseMeshTopology.h>

#include <SofaBaseTopology/TopologyData.h>

#include <SofaBaseMechanics/MechanicalObject.h>
#include "sofa/helper/system/thread/CTime.h"
#include "utils.h"
#include<iostream>
#include<fstream>


#define MAX_DOUBLE 1.797693134862315708e308
#define MIN_DOUBLE -1.797693134862315708e308


namespace sofa
{

namespace component
{

namespace engine
{
using namespace sofa::defaulttype;
using namespace sofa::component::topology;
using namespace sofa::helper::system::thread;

/*
   static bool TimeSortFunction (std::pair<double, unsigned int> ele1, std::pair<double, unsigned int> ele2)
 {
  return (ele1.first) < (ele2.first);
 }

   static bool FlagSortFunction (std::pair<int, unsigned int> ele1, std::pair<unsigned int, unsigned int> ele2)
   {
//      return (ele1.first) < (ele2.first);
   }
*/


/// Fast marching containers. Times and node indices are stored in one of these containers.
struct HeartEikonalData_opt{
   double eikonalCurvature;
   /// EIKONAL Fast-Marching: close points, computed this turn, ordered in increasing depolarisation time
   std::multimap<double, unsigned int> FMclose;
   /// EIKONAL Fast-Marching: accepted points
   std::multimap<double, unsigned int> FMaccepted;
   /// EIKONAL Fast-Marching: refractory points
   std::multimap<double, unsigned int> FMrefractory;
   /// EIKONAL Fast-Marching: changed-known points
   std::multimap<double, unsigned int> FMchangedAccepted;
   HeartEikonalData_opt() {
     eikonalCurvature = 0.0;
   }
 };


/// Specialisation of nodes to have some more informations
struct HeartVertex{
   double depolarisationTime;  //0
   double repolarisationTime;  //1
   double refractoryPeriod;    //2
   double diastolicInterval;   //3
   double actionPotentialDuration;  //4

   HeartVertex(){
      depolarisationTime = MAX_DOUBLE;
      repolarisationTime = MAX_DOUBLE;
      refractoryPeriod = 0.01;//0.01;
      diastolicInterval = 0.001;//0.7;
      actionPotentialDuration = 0.08;//0.29;//0.0483;
   }

   /// Output stream
   inline friend std::ostream& operator<< ( std::ostream& os, const HeartVertex& /*hi*/ )
   {
      return os;
   }

   /// Input stream
   inline friend std::istream& operator>> ( std::istream& in, HeartVertex& /*hi*/ )
   {
      return in;
   }
};


/** This class perform Fast Marching algorithm for tetrahedral meshes. Possibility to use Anisotropy.*/
class ZoneFMM : public sofa::core::DataEngine
{
public:
    SOFA_CLASS(ZoneFMM,  sofa::core::DataEngine);

    /// List of enum for Flag of Eikonal propagation
    enum EikonalFlag {
       // value computed
       ACCEPTED = 0,
       // narrow band
       TRIAL = 1,
       // out of the narrow band
       TOOFAR = 2,
       // in refractory period
       REFRACTORY = 3
    };
	typedef sofa::core::topology::Topology::Tetrahedron Tetrahedron;
	typedef sofa::core::topology::Topology::Triangle Triangle;
	typedef sofa::core::topology::Topology::Point Point;
	typedef sofa::core::topology::Topology::TetraID TetraID;
	typedef sofa::core::topology::Topology::Tetra Tetra;
	typedef sofa::core::topology::Topology::Edge Edge;
	typedef sofa::core::topology::BaseMeshTopology::EdgesInTriangle EdgesInTriangle;
	typedef sofa::core::topology::BaseMeshTopology::EdgesInTetrahedron EdgesInTetrahedron;
	typedef sofa::core::topology::BaseMeshTopology::TrianglesInTetrahedron TrianglesInTetrahedron;
    typedef sofa::defaulttype::Vec<3,SReal> Vec3;
    typedef std::pair<double, unsigned int> timePair;
    typedef std::pair<int, unsigned int> flagPair;

    /// -- SOFA functions API: --
    ///@{
    ZoneFMM();
    ~ZoneFMM();

    /// Init function called at the creation of the component.
    virtual void init();

    virtual void doUpdate();
    void getDistanceMap(Ref zone=0);
    void ExportToVTK(Int i, Ref zone);
    void GetPointsOnBorder(Ref zone);
    void SignDistance(Ref zone);

    /// Init function called when modifying the component GUI.
    // virtual void reinit();

    virtual void draw(const sofa::core::visual::VisualParams*);

    /// If topology change, corresponding PointData, TetraData will handle these changes.
    void handleTopologyChange();
    
    /// To handle events like key pushed events.
    void handleEvent(sofa::core::objectmodel::Event *);

    /** Solve function called at each timestep (inherit from the OdeSolverimpl frame).
      * In this Fast Marching Method, we use this function as main function. At each timestep it will
      * iterate the algorithm. Thus it is possible to follow the propagation in SOFA.
      */
//  void solve (const core::ExecParams* params, double dt, sofa::core::MultiVecCoordId xResult, sofa::core::MultiVecDerivId vResult);

    ///@}


    /// -- FMM functions API: --
    ///@{
    /// Init fastMaching method, initialisation function of FMM.
    bool initFastMarching();

    bool initPacing(double currentTime);

    /// Hack: as solve is using complicated multiVector, this function is called at each step to update the X vector.
//    void updatePropagation ();

    /// Call by solve at each timestep to perform 1 iteration of the algorythm.
    void updateActionPotential();

    /// For one vertex will look for neighbours and update their depolarisation time using computeEikonalTime
    void updateNeighbours(const unsigned int vertexIndex);

    /// Compute depolarisation time of a given vertex.
    double computeEikonalTime(const Tetrahedron tetra, const unsigned int tetraIndex, const unsigned int vertexIndex);
    ///@}


    /// Heart simulation specific: TODO move them in a specialized class!
    bool initHeartSimulation();

    bool initHeartPacing(double currentTime);

    /// -- Eikonal solve functions API: --
    ///@{
    /// Functions of minimisation called by computeEikonalTime
    double group_velocity (const unsigned int vertexIndex, const unsigned int tetraIndex, Vec3 yon);

    double minimize_Analytic2 (const unsigned int vertexIndex1, const unsigned int vertexIndex2, const unsigned int vertexIndex3, const unsigned int tetraIndex);

    bool find_roots_indic(double* R,double a,double b, double c);
    ///@}


    /// -- Eikonal solve functions API: --
    ///@{
    /// Functions for handling fibers. //TODO: think about creating a component specialized in heart.
    //{
    sofa::defaulttype::Mat3x3d getFibreBase (const unsigned int tetraIndex);

    sofa::defaulttype::Mat3x3d getLocalBase (const unsigned int tetraIndex);

    Vec3 getAnisotropicDirection(const unsigned int tetraIndex);

    Vec3 getLocalAnisotropicDirection(const unsigned int tetraIndex);

    void updateFibersDirection();
    ///@}


    /// -- Set/Get method for different Data: --
    ///@{
    void setHeartVertex (const unsigned int vertexIndex, const HeartVertex& dataVertex);
    const HeartVertex getHeartVertex(const unsigned int vertexIndex);

    void setEikonalFlag (const unsigned int vertexIndex, const EikonalFlag flag);
    EikonalFlag getEikonalFlag (const unsigned int vertexIndex);

    void setTetrahedronConductivity (const unsigned int tetraIndex, const double conductivity);
    double getTetrahedronConductivity (const unsigned int tetraIndex);

    void setTetrahedraConductivities (const sofa::helper::vector <unsigned int> tetraIndices, const sofa::helper::vector <double> conductivities);
    const sofa::helper::vector <double> getTetrahedraConductivities (const sofa::helper::vector <unsigned int> tetraIndices);

    void setNewWave (const unsigned int vertexIndex, const HeartVertex& dataVertex);

    void setNewInitWave ();

    void exportDataFunction(sofa::helper::system::thread::ctime_t startTime);
    ///@}



    // Function beeing part of the OdeSolver frame. They must be overwrittent but are not used in our case
    //{
    double getIntegrationFactor(int inputDerivative, int outputDerivative) const
    {
       const double dt = getContext()->getDt();
       double matrix[3][3] = {
          { 1, dt, 0},
          { 0, 1, 0},
          { 0, 0, 0}};
       if (inputDerivative >= 3 || outputDerivative >= 3)
          return 0;
       else
          return matrix[outputDerivative][inputDerivative];
    }

    double getSolutionIntegrationFactor(int outputDerivative) const
    {
       const double dt = getContext()->getDt();
       double vect[3] = { dt, 1, 1/dt};
       if (outputDerivative >= 3)
          return 0;
       else
          return vect[outputDerivative];
    }
    //}

 protected:

    /// Pointeurs to SOFA classes
//    sofa::component::topology::TetrahedronSetTopologyContainer* _topocontainer;
//    sofa::component::topology::TetrahedronSetGeometryAlgorithms<defaulttype::Vec3dTypes>* _topoGeo;
//    sofa::component::container::MechanicalObject<defaulttype::Vec1dTypes>* _mecaElec;
    sofa::core::topology::BaseMeshTopology* _topology;

    //Heart data structure
    HeartEikonalData_opt myData;

    /// sumarise all data in one PointData:
    /// m_depolarisationTime | m_repolarisationTime | m_refractoryPeriod | m_diastolicInterval | m_actionPotentialDuration
    PointData < helper::vector<HeartVertex> > m_dataVertices;

    PointData < helper::vector<int> > m_flagContainer; // Data doesn't support enum

    /// Number of iteration of FMM to perfom for 1 timestep of SOFA
    Data <unsigned int> m_nbrIter;

    // Init node pacing:
    Data <sofa::helper::vector <unsigned int> > m_initNodes;
    Data <sofa::helper::vector <unsigned int> > m_initNodes2;


    /// electrical longitudinal conductivity.
    Data <double> m_electricalConductivity;
    TetrahedronData <helper::vector<double> > m_tetraConductivity;

    /// electrical conductivity anisotropy ratio (longitudinal conductivity)/(transverse conductivity)
    Data <double> m_electricalAnisotropy;
    bool anisotropicFM;

    // Fibers in tetra local coordinates. This Data need to be link from the loader.
    TetrahedronData < helper::vector<sofa::defaulttype::Vec<3,SReal> > > m_tetraBFibers;
    TetrahedronData < helper::vector<sofa::defaulttype::Vec<3,SReal> > > m_tetraFibers;

    // Fibers synthetic
    Data <bool> p_useSyntheticFibers;
    Data <Vec <3, SReal> > p_fiberDirection;
    // Vector Time / index
    //sofa::helper::vector < timePair > timeContainer;
    // Vector Time / index
    //sofa::helper::vector < flagPair > flagContainer;

    /// Diplay options:
    Data <bool> _drawTime;
    Data <bool> _drawConduc;
    Data <bool> _drawInterpol;
    Data <bool> _drawFibers;
    Data <float> _fiberLength;
    std::map <unsigned int, double> realValues;

    /// Data recovered from files or MeshTetrahedrisationLoader
    Data <helper::vector <std::string> > m_loaderSurfaceZoneNames;
    Data <helper::vector < helper::vector <unsigned int> > > m_loaderSurfaceZones;
    Data <helper::vector <std::string> > m_loaderZoneNames;
    Data <helper::vector < helper::vector <unsigned int> > > m_loaderZones;

    /// Heart simulation specific: TODO move them in a specialized class!
    ///{@
    Data <bool> _heartMesh;
    Data <std::string> m_RightV;
    Data <std::string> m_LeftV;
    Data <std::string> m_scars;
    Data <std::string> m_isthmus;
    Data <double> m_isthmusConductivity;

    Data <bool> _exportData;
    sofa::core::objectmodel::DataFileName f_filename;

    sofa::helper::vector <unsigned int> RightVContainer;
    sofa::helper::vector <unsigned int> LeftVContainer;
    sofa::helper::vector <unsigned int> IsthmusContainer;
    sofa::helper::vector <unsigned int> IsthmusContainerTetra;
    ///@}

    std::ofstream* outfile;


    // Gaetan
    Data<VVReal> m_distMaps;
    Data<bool> m_exportAtEachStep;
    Data<VRef> m_zoneTetras;
    Data<VCoord> m_position;
    Data<std::string> m_importFileName;
    Data<bool> m_exportCSV;

    void exportToCSV(Ref zone);
    void importDMapFromCSV();

    inline Coord getPointPosition(Ref i)
    {
        return (m_position.getValue())[i];
    }

    Ref getZoneOf(Ref tetraID)
    {
        return (m_zoneTetras.getValue())[tetraID];
    }
};



} // namespace engine

} // namespace component

} // namespace sofa

#endif
