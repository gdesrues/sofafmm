/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 beta 4      *
*                (c) 2006-2009 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/

#include "ZoneFMM.h"
#include <sofa/simulation/MechanicalVisitor.h>
#include <sofa/core/ObjectFactory.h>
#include <math.h>
#include <iostream>
#include <SofaBaseTopology/TopologyData.inl>
#include <SofaBaseTopology/TetrahedronSetTopologyAlgorithms.h>
#include <SofaBaseTopology/PointSetGeometryAlgorithms.h>
#include <sofa/core/visual/VisualParams.h>
#include <sofa/helper/system/gl.h>
#include <sofa/core/objectmodel/Event.h>
#include <sofa/simulation/AnimateBeginEvent.h>
#include <sofa/simulation/AnimateEndEvent.h>
#include <sofa/core/objectmodel/KeypressedEvent.h>
#include <sofa/core/objectmodel/KeyreleasedEvent.h>

#include <algorithm>

//#include <sofa/defaulttype/Mat.h>
#define SOFA_TIMER_Animate = 10


namespace sofa
{

namespace component
{

namespace engine
{

using namespace sofa::defaulttype;
using namespace core::behavior;
using namespace core::topology;

ZoneFMM::ZoneFMM()
: _topology(nullptr)
, m_dataVertices( initData(&m_dataVertices,"m_dataVertices","Data to handle topology on vertices.") )
, m_flagContainer( initData(&m_flagContainer,"flagContainer","Container of Eikonal flag for each node.") )
, m_nbrIter( initData(&m_nbrIter, (unsigned int)5, "nbrIter","Number of iteration of FMM to be perfomed for 1 timestep"))
, m_initNodes( initData(&m_initNodes, "initNodes","Initial zone paced. If empty, will do nothing."))
, m_initNodes2( initData(&m_initNodes2, "initNodes2","Initial zone paced. If empty, will do nothing."))
, m_electricalConductivity( initData(&m_electricalConductivity, (double)500.0, "electricalConductivity","Global tetrahedral conductivity."))
, m_tetraConductivity( initData(&m_tetraConductivity, "tetraConductivity","Conductivity for each tetrahedron,, by default equal to electricalConductivity."))
, m_electricalAnisotropy( initData(&m_electricalAnisotropy, (double)1.0, "electricalAnisotropy","Anisotropy ratio, (longitudinal conductivity)/(transverse conductivity). Default is 1 = isotropy."))
, anisotropicFM(0)
, m_tetraBFibers( initData(&m_tetraBFibers, "tetraBFibers","Tetrahedal fibers in barycentric coordinates."))
, m_tetraFibers( initData(&m_tetraFibers, "tetraFibers","Tetrahedral fibers in world coordinates."))
, p_useSyntheticFibers( initData(&p_useSyntheticFibers, (bool)false,"useSyntheticFibers","To use simulate a single synthetic fiber direction."))
, p_fiberDirection( initData(&p_fiberDirection, "fiberDirection","To simulate a single synthetic fiber direction."))
, _drawTime( initData(&_drawTime, (bool)false,"drawTime","To display real time values instead of eikonal state."))
, _drawConduc( initData(&_drawConduc, (bool)false, "drawConduc","To display conductivity map."))
, _drawInterpol( initData(&_drawInterpol, (bool)true, "  ","To display conductivity map interpolated regarding APD duration."))
, _drawFibers( initData(&_drawFibers, (bool)false, "drawFibers","To display Fiber directions."))
, _fiberLength (initData(&_fiberLength, (float)1, "fiberLength", "Fiber length visualisation."))
, m_loaderSurfaceZoneNames (initData(&m_loaderSurfaceZoneNames, "loaderSurfaceZoneNames", "See surface zones Name loaded from MeshTetrahedrisationLoader."))
, m_loaderSurfaceZones(initData(&m_loaderSurfaceZones, "loaderSurfaceZones", "See surface zones Size loaded from MeshTetrahedrisationLoader."))
, m_loaderZoneNames (initData(&m_loaderZoneNames, "loaderZoneNames", "See zones Name loaded from MeshTetrahedrisationLoader."))
, m_loaderZones (initData(&m_loaderZones, "zones", "Zones loaded from MeshTetrahedrisationLoader."))
, _heartMesh( initData(&_heartMesh, (bool)false, "heartMesh","If true, will use meshLoader and use Scars, LV RV,..."))
, m_RightV( initData(&m_RightV, (std::string)"RV", "RightV","Rayleigh damping coefficient related to mass"))
, m_LeftV( initData(&m_LeftV, (std::string)"LV","LeftV","Rayleigh damping coefficient related to mass"))
, m_scars( initData(&m_scars, (std::string)"scars","scars","Rayleigh damping coefficient related to mass"))
, m_isthmus( initData(&m_isthmus, (std::string)"Isthmus","isthmus","Rayleigh damping coefficient related to mass"))
, m_isthmusConductivity( initData(&m_isthmusConductivity, (double)100,"isthmusConductivity","Rayleigh damping coefficient related to mass"))
, _exportData( initData(&_exportData, (bool)false, "exportData","To data at each timestep."))
, f_filename( initData(&f_filename, "filename", "output file name. Extension csv will be added."))
//, m_dataVertices( initData(&m_dataVertices, "dataVertices","Rayleigh damping coefficient related to mass"))
, outfile(NULL)
, m_distMaps(initData(&m_distMaps, "distanceMaps", "Distance maps for each zone at mesh vertices"))
, m_exportAtEachStep(initData(&m_exportAtEachStep, false, "exportAtEachStep", "if False wll export the distanceMap at the end"))
, m_zoneTetras(initData(&m_zoneTetras, "zonesTetras", "List of the zone of each tetra"))
, m_position(initData(&m_position, "position", "Material point position"))
, m_importFileName(initData(&m_importFileName, "importFileName", "if set, will not compute distanceMaps but read/write them from/to files"))
, m_exportCSV(initData(&m_exportCSV, false, "export", "if true, will write distanceMaps into m_importFileName"))
//, f_printLog(initData(&f_printLog, "printLog", "Will display infos in output file"))
{
}

ZoneFMM::~ZoneFMM(){}

void ZoneFMM::handleTopologyChange()
//THIS HAS TO BE RE-IMPLEMENTED TO HANDLE TOPOLOGY

{
//    std::list<const TopologyChange *>::const_iterator itBegin= _topocontainer->beginChange();
//    std::list<const TopologyChange *>::const_iterator itEnd=_topocontainer->endChange();

//    // PointData
//    m_flagContainer.handleTopologyEvents(itBegin, itEnd);
//    m_dataVertices.handleTopologyEvents(itBegin, itEnd);

//    // TetrahedronData
//    m_tetraBFibers.handleTopologyEvents(itBegin, itEnd);
//    m_tetraFibers.handleTopologyEvents(itBegin, itEnd);
 }


void ZoneFMM::handleEvent(sofa::core::objectmodel::Event *event)
{
   if (sofa::core::objectmodel::KeypressedEvent* ev = dynamic_cast<sofa::core::objectmodel::KeypressedEvent*>(event))
   {
      std::cout << "key pressed " << std::endl;
      switch(ev->getKey())
      {

         case 'D':
         case 'd':
         this->setNewInitWave();
            break;
      }
   }
}


void ZoneFMM::init()
{
   // Get pointers
    this->getContext()->get(_topology);
    if (!_topology)
      std::cerr << "Error: ZoneFMM is not able to acces topology!" << std::endl;

//   this->getContext()->get(_topocontainer, sofa::core::objectmodel::BaseContext::SearchRoot);
//   if (!_topocontainer)
//      std::cerr << "Error: ZoneFMM is not able to acces topology!" << std::endl;

//   this->getContext()->get(_topoGeo, sofa::core::objectmodel::BaseContext::SearchRoot);
//   if (!_topoGeo)
//      std::cerr << "Error: ZoneFMM is not able to acces topology geometry!" << std::endl;

//   this->getContext()->get(_mecaElec, sofa::core::objectmodel::BaseContext::SearchRoot);
//   if (!_mecaElec)
//      std::cerr << "Error: ZoneFMM is not able to acces state container (mechanicalObject)!" << std::endl;

   // Force creation of TetrahedraAroundVertexArray here so won't disturb first iteration.
//   _topology->getTetrahedraAroundVertexArray();


   // Init anisotropy
   if (m_electricalAnisotropy.getValue() != 1.0) //anisotropic FMM, check if fibers are presents.
   {
      if ( (m_tetraFibers.getValue()).size() != (unsigned int)_topology->getNbTetrahedra())
      {
         if (p_useSyntheticFibers.getValue())
            anisotropicFM = true;
         else
            std::cerr << "Error: ZoneFMM using anisotropy, but fiber container has not an appropriate size!" << std::endl;
      }
      else
      {
         //std::cout <<"DEBUG CHEK: using anisotropy!!!!" <<std::endl;
         anisotropicFM = true;
      }
   }


   // Init export DAta
   if (_exportData.getValue() )
   {
      std::cout << "Number of ticks per sec on this computer: " << CTime::getTicksPerSec() << std::endl;
      const std::string& filename = f_filename.getValue();
      if (!filename.empty())
      {
         outfile = new std::ofstream(filename.c_str());
         if( !outfile->is_open() )
         {
            serr << "Error creating file "<<filename<<sendl;
            delete outfile;
            outfile = NULL;
         }
      }
      else
         std::cerr << "Error: MeshExporter can't save file, filename is empty" << std::endl;
   }

   // Init conductivity
   unsigned int nbrTetra = _topology->getNbTetrahedra();
   sofa::helper::vector <double>& tetraCon = *(m_tetraConductivity.beginEdit());
   const double& conduc = m_electricalConductivity.getValue();
   tetraCon.resize(nbrTetra);
   for (unsigned int i=0; i<nbrTetra; ++i)
      tetraCon[i] = conduc;
   m_tetraConductivity.endEdit();


//   // Init Eikonal flag/containers and status:
//   bool _init = this->initFastMarching();
//   if (!_init)
//      std::cerr << "Error: ZoneFMM: Fast Marching Method initialisation failed" << std::endl;

//   // Init heart simulation:
//   if (_heartMesh.getValue())
//   {
//      bool _init = this->initHeartSimulation();
//      if (!_init)
//         std::cerr << "Error: ZoneFMM: heart simulation initialisation failed" << std::endl;
//   }



   // Create vector m_zoneTetras : m_zoneTetras[i] contains the zone of the tetrahedra i
    raZones zones(m_loaderZones);
    helper::WriteOnlyAccessor<Data<VRef>> zoneTetras(m_zoneTetras);
    zoneTetras.resize(_topology->getNbTetras());
    for (Ref zone=0; zone<zones.size(); ++zone)
       for (Ref tetraID : zones[zone])
           zoneTetras[tetraID] = zone;


//   this->updatePropagation();
    if(m_importFileName.isSet() and !m_exportCSV.getValue())
        this->importDMapFromCSV();
    else
        doUpdate();
}



bool ZoneFMM::initHeartSimulation()
{
   // Init surface zones
   const sofa::helper::vector<std::string>& names = m_loaderSurfaceZoneNames.getValue();
   unsigned int cpt = 0;
   for (unsigned int i = 0; i<names.size(); ++i )
   {
      if (names[i] == m_RightV.getValue())
      {
         sofa::helper::vector <unsigned int> tmp = (m_loaderSurfaceZones.getValue())[i];
         for (unsigned int j = 0; j<tmp.size(); ++j)
         {
            Triangle tri = _topology->getTriangle( tmp [j]);


            for (unsigned int p = 0; p<3; ++p)
            {
               bool find = false;
               for (unsigned int k =0; k<RightVContainer.size(); ++k)
                  if ( tri [p] == RightVContainer[k])
                  {
                     find = true;
                     break;
                  }

               if (!find)
                  RightVContainer.push_back(tri[p]);
            }
         }
         cpt++;
      }

      if (names[i] == m_LeftV.getValue())
      {
         sofa::helper::vector <unsigned int> tmp = (m_loaderSurfaceZones.getValue())[i];
         for (unsigned int j = 0; j<tmp.size(); ++j)
         {
            Triangle tri = _topology->getTriangle( tmp [j]);


            for (unsigned int p = 0; p<3; ++p)
            {
               bool find = false;
               for (unsigned int k =0; k<LeftVContainer.size(); ++k)
                  if ( tri [p] == LeftVContainer[k])
                  {
                     find = true;
                     break;
                  }

               if (!find)
                  LeftVContainer.push_back(tri[p]);
            }
         }
         cpt++;
      }

      if (cpt == 2)
         break;
   }

   if (LeftVContainer.empty() && RightVContainer.empty())
   {
      std::cerr << "Error: Initialisation of Right and left ventricles failed." <<std::endl;
      //LeftVContainer.push_back(0);
      RightVContainer.push_back(0);
   }


   // scars & Isthmus init
   const sofa::helper::vector<std::string>& Znames = m_loaderZoneNames.getValue();
   sofa::helper::vector <double>& tetraCon = *(m_tetraConductivity.beginEdit());
   double istmusConduc = m_isthmusConductivity.getValue();
   cpt = 0;
   for (unsigned int i = 0; i<Znames.size(); ++i )
   {
      if (Znames[i] == m_scars.getValue())
      {
         std::cout <<"init scars!" << std::endl;
         sofa::helper::vector <unsigned int> tmp = (m_loaderZones.getValue())[i];
         for (unsigned int j=0; j<tmp.size(); ++j)
           tetraCon[ tmp [j]] = 0.0;

         cpt++;
      }
      else if (Znames[i] == m_isthmus.getValue())
      {
         std::cout <<"init Isthmus!" << std::endl;
         IsthmusContainerTetra = (m_loaderZones.getValue())[i];
         for (unsigned int j=0; j<IsthmusContainerTetra.size(); ++j)
           tetraCon[ IsthmusContainerTetra [j]] = istmusConduc;

         for (unsigned int j=0; j<IsthmusContainerTetra.size(); ++j)
         {
            Tetrahedron tetra = _topology->getTetrahedron(IsthmusContainerTetra [j]);

            for (unsigned int p = 0; p<4; ++p)
            {
               bool find = false;
               for (unsigned int k =0; k<IsthmusContainer.size(); ++k)
                  if ( tetra [p] == IsthmusContainer[k])
                  {
                     find = true;
                     break;
                  }

               if (!find)
                  IsthmusContainer.push_back(tetra[p]);
            }
         }

         cpt++;
      }

      if (cpt == 2)
         break;
   }
   m_tetraConductivity.endEdit();
   return true;
}

bool ZoneFMM::initHeartPacing(double currentTime)
{
   /// flag initialisation:
   //init of LV et RV
   helper::vector <int>& my_flagContainer = *(m_flagContainer.beginEdit());
   sofa::helper::vector < HeartVertex >& my_dataVertices = *(m_dataVertices.beginEdit());

   for (unsigned int i =0; i<LeftVContainer.size(); ++i)
   {
      unsigned int vertex = LeftVContainer[i];
      my_dataVertices[ vertex ].depolarisationTime = currentTime;
      my_flagContainer[ vertex ] = ZoneFMM::ACCEPTED;
      (myData.FMaccepted).insert(std::pair<double, unsigned int> (currentTime, vertex));

      if (_drawTime.getValue())
         realValues.insert( std::pair <unsigned int, double> (vertex, currentTime));
   }
   for (unsigned int i =0; i<LeftVContainer.size(); ++i)
      this->updateNeighbours(LeftVContainer[i]);

   for (unsigned int i =0; i<RightVContainer.size(); ++i)
   {
      unsigned int vertex = RightVContainer[i];
      my_dataVertices[ vertex ].depolarisationTime = currentTime;
      my_flagContainer[ vertex ] = ZoneFMM::ACCEPTED;
      (myData.FMaccepted).insert(std::pair<double, unsigned int> (currentTime, vertex));

      if (_drawTime.getValue())
         realValues.insert( std::pair <unsigned int, double> (vertex, currentTime));
   }
   for (unsigned int i =0; i<RightVContainer.size(); ++i)
      this->updateNeighbours(RightVContainer[i]);

   m_dataVertices.endEdit();
   m_flagContainer.endEdit();

   return true;
}


bool ZoneFMM::initPacing(double currentTime)
{
   const helper::vector <unsigned int>& my_InitNodes = m_initNodes.getValue();
   if (my_InitNodes.empty())
      return true;

   helper::vector <int>& my_flagContainer = *(m_flagContainer.beginEdit());
   sofa::helper::vector < HeartVertex >& my_dataVertices = *(m_dataVertices.beginEdit());

   for (unsigned int i =0; i<my_InitNodes.size(); ++i)
   {
      unsigned int vertex = my_InitNodes[i];
      my_dataVertices[ vertex ].depolarisationTime = currentTime;
      my_flagContainer[ vertex ] = ZoneFMM::ACCEPTED;
      (myData.FMaccepted).insert(std::pair<double, unsigned int> (currentTime, vertex));

      if (_drawTime.getValue())
         realValues.insert( std::pair <unsigned int, double> (vertex, currentTime));
   }
   for (unsigned int i =0; i<my_InitNodes.size(); ++i)
      this->updateNeighbours(my_InitNodes[i]);

   m_dataVertices.endEdit();
   m_flagContainer.endEdit();

   return true;
}


bool ZoneFMM::initFastMarching()
{
   // This function initialise simple Fast Marching method
   int nbrVertices = _topology->getNbPoints();

   sofa::helper::vector < HeartVertex >& my_dataVertices = *(m_dataVertices.beginEdit());
   helper::vector <int>& my_flagContainer = *(m_flagContainer.beginEdit());

   my_dataVertices.resize(nbrVertices);
   my_flagContainer.resize(nbrVertices);

   for (int i = 0; i<nbrVertices; ++i)
      my_flagContainer[i] = ZoneFMM::TOOFAR;

   m_dataVertices.endEdit();
   m_flagContainer.endEdit();

   if (_heartMesh.getValue())
      this->initHeartPacing(0.0);
   else
      this->initPacing(0.0);

   return true;
}



/*
void ZoneFMM::reinit()
{
    this->init();
   std::cout << "ZoneFMM::reinit()" << std::endl;
  // this->initFastMarching();
}
*/


void ZoneFMM::doUpdate()
{
    bool computeBorders = !m_initNodes.isSet();

    helper::WriteOnlyAccessor<Data<VVReal>> dMap(m_distMaps);
    Int nbZones = (m_loaderZones.isSet()) ? m_loaderZones.getValue().size() : 1;
    dMap.resize(nbZones);
    if(f_printLog.getValue()) Print("[ZoneFMM] Found",nbZones,"zones.");

    for(Ref zone=0; zone<nbZones; ++zone)
    {
        if(computeBorders) this->GetPointsOnBorder(zone);

        this->initFastMarching();

        this->getDistanceMap(zone);

        if(m_exportCSV.getValue()) exportToCSV(zone);
    }

}


void ZoneFMM::exportToCSV(Ref zone)
{
    if(m_importFileName.isSet())
    {
        Int nbPoints = _topology->getNbPoints();
        std::ofstream ofs;
        std::string fName = m_importFileName.getValue();
        if(m_loaderZones.isSet()) fName.insert(fName.find(".csv"), "_zone"+std::to_string(zone));
        ofs.open(fName, std::ofstream::out);

        ofs << nbPoints << std::endl;

        helper::ReadAccessor<Data<helper::vector<HeartVertex>>> my_dataVertices(m_dataVertices);
        for(Int i=0; i<nbPoints; ++i)
        {
            Real c=my_dataVertices[i].depolarisationTime;
            c = (c>100) ? 0 : c;
            ofs << c << std::endl;
        }

        ofs.close();
        if(f_printLog.getValue()) Print("[ZoneFMM](exportToCSV) Distance map for zone",zone,"exported");
    }
}


void ZoneFMM::importDMapFromCSV()
{
    Int nbPoints = _topology->getNbPoints();
    wadistanceMap dMap(m_distMaps); dMap.resize(m_loaderZones.getValue().size());

    for(Ref zone=0; zone<m_loaderZones.getValue().size(); ++zone)
    {
        dMap[zone].resize(nbPoints);
        std::ifstream ofs;
        std::string fName = m_importFileName.getValue();
        if(m_loaderZones.isSet()) fName.insert(fName.find(".csv"), "_zone"+std::to_string(zone));
        ofs.open(fName, std::ifstream::in);

        Int nbPtsFile=0;
        ofs >> nbPtsFile;
        if(nbPtsFile!=nbPoints) Print("File not good, number of points don't match : "+std::to_string(nbPoints)+" != "+std::to_string(nbPtsFile));
        else for(Int i=0; i<nbPoints; ++i) ofs >> dMap[zone][i];

        ofs.close();
        if(f_printLog.getValue()) Print("[ZoneFMM](importDMapFromCSV) Zone",zone,"imported");
    }
}


void ZoneFMM::GetPointsOnBorder(Int zone)
{
    waVRef initNodes(m_initNodes);
    initNodes.clear();

    if(m_loaderZones.getValue().size()>1)
    {
        raZones tetrasByZone(m_loaderZones);
        VBool isBorder(_topology->getNbPoints()); isBorder.fill(false);
        for (Ref t : tetrasByZone[zone])
            for (Ref p : _topology->getTetra(t))
                if(not isBorder[p])
                {
                    VRef tetrasAround = _topology->getTetrahedraAroundVertex(p);
                    Int ref = getZoneOf(tetrasAround[0]);
                    for (Ref tt : tetrasAround)
                        if(getZoneOf(tt) != ref)
                            isBorder[p] = true;
                }
        for (Ref i=0; i<_topology->getNbPoints(); ++i)
            if(isBorder[i]) initNodes.push_back(i);
    }
    else
    {
        for(Ref i=0; i<_topology->getNbPoints(); ++i)
        {
//            if(std::abs(_topology->getPX(i)) < 0.01)
            Coord c = this->getPointPosition(i);//Coord(_topology->getPX(i), _topology->getPY(i), _topology->getPZ(i));
            if(c==Coord(0,0,0))
                initNodes.push_back(i);
        }
    }

//    Print("Computed points on border", initNodes.size());
    if(initNodes.empty()) msg_error() << "[ZoneFMM_old(GetDistanceMap)] No points were found on border.";
}


void ZoneFMM::getDistanceMap(Ref zone)
{
    Int g=0;
    if(m_exportAtEachStep.getValue()) ExportToVTK(g, zone);
    while (!(myData.FMclose).empty() || !(myData.FMchangedAccepted).empty())
    {
        this->updateActionPotential();
//        this->updatePropagation();

        ++g;
        if(m_exportAtEachStep.getValue()) ExportToVTK(g, zone);
    }
    this->SignDistance(zone);
    if(!m_exportAtEachStep.getValue()) this->ExportToVTK(0, zone);


    // Save distance Map to data
    helper::WriteOnlyAccessor<Data<VVReal>> dMap(m_distMaps);
    dMap[zone].resize(_topology->getNbPoints());

    helper::ReadAccessor<Data<helper::vector<HeartVertex>>> my_dataVertices(m_dataVertices);
    for(Int i=0; i<_topology->getNbPoints(); ++i)
        dMap[zone][i] = my_dataVertices[i].depolarisationTime;

    if(f_printLog.getValue()) Print("[ZoneFMM](getDistanceMap) Zone",zone,"computed");
}


void ZoneFMM::SignDistance(Ref zone)
{
    if(m_loaderZones.isSet())
    {
        helper::WriteAccessor<Data<helper::vector<HeartVertex>>> my_dataVertices(m_dataVertices);
        raZones zones(m_loaderZones);
        for (Ref tetraID : zones[zone])
          for (Ref pt : _topology->getTetra(tetraID))
              my_dataVertices[pt].depolarisationTime = -std::abs(my_dataVertices[pt].depolarisationTime);
    }
}


void ZoneFMM::ExportToVTK(Int i, Ref zone)
{
    if(!_exportData.getValue())
    {
        Int nbPoints = _topology->getNbPoints();
        std::ofstream ofs;
        std::string fName = f_filename.getValue();
        if(m_exportAtEachStep.getValue()) fName.insert(fName.find(".vtk"), "_it"+std::to_string(i));
        if(m_loaderZones.isSet()) fName.insert(fName.find(".vtk"), "_zone"+std::to_string(zone));
        ofs.open(fName, std::ofstream::out);

        ofs << "# vtk DataFile Version 2.0" << std::endl;
        ofs << "ShapeFunction Values" << std::endl;
        ofs << "ASCII" << std::endl;

        ofs << std::endl << "DATASET UNSTRUCTURED_GRID" << std::endl;
        ofs << "POINTS " << nbPoints << " float" << std::endl;
        for(Ref i=0; i<nbPoints; ++i) ofs << this->getPointPosition(i) << std::endl;

        Int nbTetras = _topology->getNbTetrahedra();
        ofs << std::endl << "CELLS " << nbTetras << " " << 5*nbTetras << std::endl;
        for(Int i=0; i<nbTetras; ++i) ofs << "4 " << _topology->getTetra(i) << std::endl;

        ofs << std::endl << "CELL_TYPES " << nbTetras << std::endl;
        for(Int i=0; i<nbTetras; ++i) ofs << "10" << std::endl;

        ofs << std::endl << "POINT_DATA " << nbPoints << std::endl;
        ofs << "SCALARS zone float" << std::endl;
        ofs << "LOOKUP_TABLE default" << std::endl;
        helper::ReadAccessor<Data<helper::vector<HeartVertex>>> my_dataVertices(m_dataVertices);
        for(Int i=0; i<nbPoints; ++i)
        {
            Real c=my_dataVertices[i].depolarisationTime;
            c = (c>10) ? 0 : c;
            ofs << c << std::endl;
        }

        ofs.close();
    }
}




void ZoneFMM::setNewInitWave ()
{
   helper::vector <int>& my_flagContainer = *(m_flagContainer.beginEdit());
   for (unsigned int i = 0; i<my_flagContainer.size(); ++i)
      my_flagContainer[i] = ZoneFMM::TOOFAR;
   m_flagContainer.endEdit();

   double simuTime = this->getContext()->getTime();
   simuTime += 0.0000001; // small hack

   if (_heartMesh.getValue())
      this->initHeartPacing(simuTime);
   else
      this->initPacing(simuTime);

//   this->updatePropagation();
}


//void ZoneFMM::updatePropagation()
//{
//   // Quick hack for multiVector handle: this function will overwrite infos in mechanicalObject
//    Data<sofa::helper::vector<sofa::defaulttype::Vec<1,SReal> >  >* ccoords = _mecaElec->write(core::VecCoordId::position());
//    sofa::helper::vector<sofa::defaulttype::Vec<1,SReal> > & coords= *((*ccoords).beginEdit());

//   if (_drawTime.getValue())
//   {
//      std::map <unsigned int, double>::iterator itm;

//      for (itm = realValues.begin(); itm != realValues.end(); ++itm)
//         coords[ itm->first] = itm->second;
//   }
//   else
//   {
//      if (_drawInterpol.getValue()) // Recompute values by interpolating on time.
//      {
//         const sofa::helper::vector < HeartVertex >& my_dataVertices = m_dataVertices.getValue();
//         unsigned int pos = 0;
//         double posD = 0.0;

//         for (unsigned int i = 0; i<coords.size(); ++i)
//         {
//            double depolT = my_dataVertices[i].depolarisationTime;

//            if (depolT == MAX_DOUBLE) // repolarised point
//            {
//               coords[i] = 0.0;
//               continue;
//            }

//            double simuTime = this->getContext()->getTime();
//            posD = (simuTime - depolT) * 9000 / (my_dataVertices[i].actionPotentialDuration + my_dataVertices[i].refractoryPeriod);

//            if (posD < 0.0) // should not happend...
//               continue;

//             pos = std::abs (posD);

//            if (pos < 8000 )
//               coords[i] = ActionPotentialValues[pos];
//            else if (pos < 9000)
//               coords[i] = PotentialRefractoryValues[pos - 8000];
//            else
//               std::cerr << "Error: potential position out of refcurves range." << std::endl;
//         }
//      }
//      else
//      {
//         std::multimap<double, unsigned int>::iterator itm;
//         for (unsigned int i = 0; i<coords.size(); ++i)
//            coords[i] = 0.0;

//         for (itm = (myData.FMaccepted).begin(); itm != (myData.FMaccepted).end(); ++itm)
//            coords[ itm->second] = 1.0;

//         for (itm = (myData.FMrefractory).begin(); itm != (myData.FMrefractory).end(); ++itm)
//            coords[ itm->second] = 0.5;
//      }
//   }
//}


void ZoneFMM::updateActionPotential()
{
   // on remplace le while par un seul passage, l'appel via la fonction solve remplace la boucle while jusqu'a convergence.

   std::multimap <double, unsigned int>::iterator itm;
   while (!(myData.FMchangedAccepted).empty())
   {
      itm = myData.FMchangedAccepted.begin();
      this->updateNeighbours( itm->second);
      myData.FMchangedAccepted.erase(itm);
   }

   if ( !(myData.FMclose).empty())
   {
      itm = (myData.FMclose).begin();

      helper::vector <int>& my_flagContainer = *(m_flagContainer.beginEdit());
      my_flagContainer [(int)(itm->second)] = (int)ZoneFMM::ACCEPTED;
      m_flagContainer.endEdit();

      (myData.FMaccepted).insert(std::pair<double, unsigned int> (itm->first, itm->second));

      if (_drawTime.getValue())
         realValues.insert( std::pair <unsigned int, double> (itm->second, itm->first));

      this->updateNeighbours(itm->second);
      (myData.FMclose).erase(itm);
   }
}


void ZoneFMM::updateNeighbours(const unsigned int vertexIndex)
{
   //ctime_t startTime = CTime::getFastTime();

   int newIndex;
   double Tnew;
   //HeartTetraVertex3D* A=0;

   // Acces to Data member
   helper::vector <int>& my_flagContainer = *(m_flagContainer.beginEdit());
   sofa::helper::vector < HeartVertex >& my_dataVertices = *(m_dataVertices.beginEdit());

   // go through the neighbouring tetrahedra
   const sofa::helper::vector <unsigned int>& tetraAroundVertex = _topology->getTetrahedraAroundVertex(vertexIndex);

   for (unsigned int i = 0; i<tetraAroundVertex.size(); ++i)
   {
      //if (getElectricalConductivity()!=0.0)
      // get index of considered vertex
      unsigned int tetraIndex = tetraAroundVertex[i];
      Tetrahedron tetra = _topology->getTetrahedron( tetraIndex);

      // New Test for scar handle
      if (m_tetraConductivity.getValue()[tetraIndex] == 0.0)
         continue;

      int indexInTetra =  _topology->getVertexIndexInTetrahedron(tetra, vertexIndex );

      // go through other nodes
      for (unsigned int j = 1; j<4; ++j)
      {
         newIndex = tetra[ (indexInTetra +j)%4 ];
         // compute the new time of this vertex
         Tnew = this->computeEikonalTime (tetra, tetraIndex, newIndex);

         // go through the flags possibilities
         EikonalFlag flag = (EikonalFlag)m_flagContainer.getValue()[newIndex];
         double depolTime = m_dataVertices.getValue()[newIndex].depolarisationTime;


         double simuTime = this->getContext()->getTime();
         if (Tnew  < simuTime - 0.1 /* - heart periode => set a 0 si not multifront*/)
            Tnew = simuTime;

         switch (flag)
         {
         case ACCEPTED:
            if (Tnew < depolTime)
            {
               my_dataVertices [newIndex].depolarisationTime = Tnew;
               myData.FMchangedAccepted.insert(std::pair<double, unsigned int>(Tnew, newIndex));
               //v->incrementVisited();  ????
            }
            break;
         case TRIAL:
            if (Tnew < depolTime)
            {
               my_dataVertices [newIndex].depolarisationTime = Tnew;
               std::multimap<double, unsigned int>::iterator itm;
               for (itm = (myData.FMclose).begin(); itm != (myData.FMclose).end(); ++itm)
                  if (itm->second == (unsigned int)newIndex)
                  {
                   (myData.FMclose).erase(itm);
                   break;
                  }
               (myData.FMclose).insert(std::pair <double, unsigned int>(Tnew, newIndex));
            }
            break;
         case TOOFAR:
            {
               my_dataVertices [newIndex].depolarisationTime = Tnew;
               my_flagContainer [newIndex] = (int)ZoneFMM::TRIAL;
               (myData.FMclose).insert(std::pair <double, unsigned int>(Tnew, newIndex));
               break;
            }
         case REFRACTORY:
            break;
         default:
            std::cout << "Error in updateNeighbours, vertex index: " << newIndex << " doesn't have a flag." << std::endl;
            break;
         }

      }
   }

   m_flagContainer.endEdit();
   m_dataVertices.endEdit();

   //std::cout << "diff: " << CTime::getFastTime() - startTime << std::endl;
}


double ZoneFMM::computeEikonalTime(Tetrahedron tetra, unsigned int tetraIndex, unsigned int vertexIndex)
{
   //ctime_t startTime = CTime::getFastTime();
   double temp = MAX_DOUBLE, vg;
   int vertexID[4]; // vertexIndex, A, B, C
   double Times[4];
   bool flags[4];
   double norms[3];
   sofa::defaulttype::Vec<3,SReal> coords[4];

   // init
   for (unsigned int i=0; i<4; ++i)
   {
      flags[i] = true;
      Times[i] = MAX_DOUBLE;
   }

   vertexID[0] = vertexIndex;
   int indexInTetra = _topology->getVertexIndexInTetrahedron(tetra, vertexIndex);
   //std::cout << "diff_int_01: " << CTime::getFastTime() - startTime << std::endl;
   if (indexInTetra == 0 || indexInTetra == 2)
   {
      for (unsigned int i=1; i<4; ++i)
         vertexID[i] = tetra [(indexInTetra + i)%4];
   }
   else
   {
      vertexID[1] = tetra [ (indexInTetra + 1)%4 ];
      vertexID[2] = tetra [ (indexInTetra + 3)%4 ];
      vertexID[3] = tetra [ (indexInTetra + 2)%4 ];
   }

   for (unsigned int i=0; i<4; ++i)
      coords[i] =this->getPointPosition(vertexID[i]);
   //std::cout << "diff_int_02: " << CTime::getFastTime() - startTime << std::endl;
   for (unsigned int i=0; i<3; ++i)
   {
      coords[i+1] = coords[0] - coords[i+1];
      norms[i] = coords[i+1].norm();
   }

   //std::cout << "coords: " << coords[0] <<" " << coords[1] <<" " << coords[2] <<" " << coords[3] << std::endl;

   double F = 1;//m_tetraConductivity.getValue()[tetraIndex] / (1.0 + myData.eikonalCurvature/* *tetraIndex->getFrontCurvature()*/);
   if (F == 0.0)
      std::cout << "Conductivity NULL" << std::endl;

   sofa::defaulttype::Mat3x3d D;
   D.identity();
   //std::cout << "diff_int_03: " << CTime::getFastTime() - startTime << std::endl;
   if (anisotropicFM) //Anisotropic Part
   {
      Print("You should update the component to use anisotropy");
      D[1][1] = m_electricalAnisotropy.getValue();
      D[2][2] = m_electricalAnisotropy.getValue();

      sofa::defaulttype::Mat3x3d Df;
      Df = this->getFibreBase(tetraIndex);
      //std::cout << "diff_int_04: " << CTime::getFastTime() - startTime << std::endl;
      D = Df * D * Df.transposed();
      //std::cout << "base: " << D << std::endl;
   }

   //std::cout << "diff_int_1: " << CTime::getFastTime() - startTime << std::endl;

   sofa::defaulttype::Mat3x3d P = sofa::defaulttype::Mat3x3d(coords[1], coords[2], coords[3]); // par lignes
   //P.transpose(); pas besoin du coups?
   sofa::defaulttype::Mat3x3d Pinv;
   bool isInversed = invertMatrix(Pinv, P);
   if (!isInversed)
      std::cout << "Error: in computeEikonalTime, can't inverse Matrix." << std::endl;

   helper::vector <int>& my_flagContainer = *(m_flagContainer.beginEdit());
   sofa::helper::vector < HeartVertex >& my_dataVertices = *(m_dataVertices.beginEdit());

   for (unsigned int i=1; i<4; ++i)
   {
      if (my_flagContainer[ vertexID[i] ] != ACCEPTED)
         flags[i] = false;
      else
         Times[i] = my_dataVertices [ vertexID[i] ].depolarisationTime;
   }

   //std::cout << "diff_int_2: " << CTime::getFastTime() - startTime << std::endl;
   // if three points are known
   if (flags[1] && flags[2] && flags[3])
   {
      Vec3 K = P*Vec3(Times[1]/norms[0], Times[2]/norms[1], Times[3]/norms[1]);
      Vec3 L = P*Vec3(1.0/norms[0], 1.0/norms[1], 1.0/norms[2]);
      double w1 = dot(L,D*L);
      double w2 = -2.0*dot(L,D*K);
      double w3 = dot(K,D*K) - 1.0/(F*F);

      double R[2];
      bool flag_imag = find_roots_indic(R, 100.0*w1, 100.0*w2, 100.0*w3);
  // doing the update regularly if the roots are ok.
      if(!flag_imag && R[0] > 0.0) {
        Vec3 dT = P*Vec3((R[0]-Times[1])/norms[0], (R[0]-Times[2])/norms[1], (R[0]-Times[3])/norms[2]);
        Vec3 d = D*dT;
        double dir1 = dot(d,cross(coords[1], coords[2]));
        double dir2 = dot(d,cross(coords[2], coords[3]));
        double dir3 = dot(d,cross(coords[3], coords[1]));
        // if d is strictly inside the tetrahedron, the dot product with external normals must be negative
        if(dir1<0 && dir2<0 && dir3<0)
           temp = std::min(temp, R[0]);
      }
      if(!flag_imag && R[1] > 0.0) {
        Vec3 dT = P*Vec3((R[1]-Times[1])/norms[0], (R[1]-Times[2])/norms[1], (R[1]-Times[3])/norms[2]);
        Vec3 d = D*dT;
        double dir1 = dot(d,cross(coords[1], coords[2]));
        double dir2 = dot(d,cross(coords[2], coords[3]));
        double dir3 = dot(d,cross(coords[3], coords[1]));
        // if d is strictly inside the tetrahedron, the dot product with external normals must be negative
        if(dir1<0 && dir2<0 && dir3<0)
           temp = std::min(temp, R[1]);
      }

      // compute the faces solutions in case the solution is in one of them
      temp = std::min(temp, minimize_Analytic2(vertexID[1], vertexID[2], vertexID[0], tetraIndex));
      temp = std::min(temp, minimize_Analytic2(vertexID[2], vertexID[3], vertexID[0], tetraIndex));
      temp = std::min(temp, minimize_Analytic2(vertexID[3], vertexID[1], vertexID[0], tetraIndex));
   }


   // if two points are known
   else if (!flags[1] && flags[2] && flags[3])
   {
      temp = std::min(temp, minimize_Analytic2(vertexID[2], vertexID[3], vertexID[0], tetraIndex));
   }
   else if (flags[1] && !flags[2] && flags[3])
   {
      temp = std::min(temp, minimize_Analytic2(vertexID[3], vertexID[1], vertexID[0], tetraIndex));
   }
   else if (flags[1] && flags[2] && !flags[3])
   {
      temp = std::min(temp, minimize_Analytic2(vertexID[1], vertexID[2], vertexID[0], tetraIndex));
   }

   // if one point is known
   else if (flags[1] && !flags[2] && !flags[3])
   {
      vg = group_velocity( vertexID[0], tetraIndex, coords[1]);
      temp = std::min( temp, Times[1]+norms[0]/vg );
   }
   else if (!flags[1] && flags[2] && !flags[3])
   {
      //vg = group_velocity(V,b);
      //temp = min( temp, Tb+d_b/vg );
      vg = group_velocity( vertexID[0], tetraIndex, coords[2]);
      temp = std::min( temp, Times[2]+norms[1]/vg );
   }
   else if (!flags[1] && !flags[2] && flags[3])
   {
      //vg = group_velocity(V,c);
      //temp = min( temp, Tc+d_c/vg );
      vg = group_velocity( vertexID[0], tetraIndex, coords[3]);
      temp = std::min( temp, Times[3]+norms[2]/vg );
   }

   m_flagContainer.endEdit();
   m_dataVertices.endEdit();

   //std::cout << "diff_int_fin: " << CTime::getFastTime() - startTime << std::endl;
   return (temp);
}

double ZoneFMM::group_velocity(unsigned int vertexIndex, unsigned int tetraIndex, Vec3 yon)
{
   (void)vertexIndex;
   double detD;
   double F = m_tetraConductivity.getValue()[tetraIndex] / (1.0 + myData.eikonalCurvature/* *tetraIndex->getFrontCurvature()*/);

   sofa::defaulttype::Mat3x3d D;
   D.identity();

   if (anisotropicFM) //Anisotropic Part
   {
      D[1][1] = m_electricalAnisotropy.getValue();
      D[2][2] = m_electricalAnisotropy.getValue();

      sofa::defaulttype::Mat3x3d Df;
      Df = this->getFibreBase(tetraIndex);
      D = Df * D * Df.transposed();
   }

   double d11 = D[0][0];
   double d12 = D[0][1];
   double d13 = D[0][2];
   double d22 = D[1][1];
   double d23 = D[1][2];
   double d33 = D[2][2];

   detD = d11*(d22*d33 - d23*d23) - d12*(d12*d33 - d13*d23) + d13*(d12*d23 - d22*d13);
   //CERR <<"determinant:"<<detD<<ENDL;

   double x_tilda[3];
   double X[3];
   x_tilda[0] = yon[0]*(d22*d33-d23*d23)/detD + yon[1]*(d13*d23-d12*d33)/detD + yon[2]*(d12*d23 - d22*d13)/detD;
   x_tilda[1] = yon[0]*(d23*d13-d12*d33)/detD + yon[1]*(d11*d33-d13*d13)/detD + yon[2]*(d13*d12 - d11*d23)/detD;
   x_tilda[2] = yon[0]*(d12*d23-d22*d13)/detD + yon[1]*(d12*d13-d11*d23)/detD + yon[2]*(d11*d22 - d12*d12)/detD;

   //cout<<"(x,y,z):"<<"("<<x_tilda[0]<<","<<x_tilda[1]<<","<<x_tilda[2]<<")"<<endl;
   double A = sqrt(d11*x_tilda[0]*x_tilda[0] + d22*x_tilda[1]*x_tilda[1] + d33*x_tilda[2]*x_tilda[2] + 2.0*d12*x_tilda[0]*x_tilda[1] + 2.0*d13*x_tilda[0]*x_tilda[2] + 2.0*d23*x_tilda[1]*x_tilda[2])*F;

   X[0] = x_tilda[0]/A;
   X[1] = x_tilda[1]/A;
   X[2] = x_tilda[2]/A;

   double B = sqrt(d11*X[0]*X[0] + d22*X[1]*X[1] + d33*X[2]*X[2] + 2.0*d12*X[0]*X[1] + 2.0*d13*X[0]*X[2] + 2.0*d23*X[1]*X[2]);
   //CERR << "B " << B << ENDL;
   //std::cout << "ZoneFMM::group_velocity()_end" << std::endl;
   return(sqrt((X[0]*d11+X[1]*d12+X[2]*d13)*(X[0]*d11+X[1]*d12+X[2]*d13) + (X[0]*d12+X[1]*d22+X[2]*d23)*(X[0]*d12+X[1]*d22+X[2]*d23) + (X[0]*d13+X[1]*d23+X[2]*d33)*(X[0]*d13+X[1]*d23+X[2]*d33))*F/B);

}



double ZoneFMM::minimize_Analytic2(unsigned int vertexA, unsigned int vertexB, unsigned int vertexC, unsigned int tetraIndex)
{
//   sofa::component::topology::TetrahedronSetGeometryAlgorithms<defaulttype::Vec3dTypes>* _topoGeo;
//   this->getContext()->get(_topoGeo, sofa::core::objectmodel::BaseContext::SearchRoot);

//   if (!_topoGeo)
//      std::cerr << "Error: ZoneFMM is not able to acces topology geometry!" << std::endl;

   double Ta = m_dataVertices.getValue()[vertexA].depolarisationTime;
   double Tb = m_dataVertices.getValue()[vertexB].depolarisationTime;
   Vec3 a = this->getPointPosition (vertexA) - this->getPointPosition (vertexC);
   Vec3 b = this->getPointPosition (vertexB) - this->getPointPosition (vertexC);
   double d_a = a.norm();
   double d_b = b.norm();
   a /= d_a;
   b /= d_b;

   bool flag_imag;
   double t, A1, A2, B1, B2, C1, C2, f_atZero, f_atOne, f_q1, f_q2, f, detD;
   double temp1,temp2;
   double w[3], q[2], P[3][3], vec[3];
   double F = m_tetraConductivity.getValue()[tetraIndex] / (1.0 + myData.eikonalCurvature/* *tetraIndex->getFrontCurvature()*/);

   sofa::defaulttype::Mat3x3d D;
   D.identity();

   if (anisotropicFM) //Anisotropic Part
   {
      D[1][1] = m_electricalAnisotropy.getValue();
      D[2][2] = m_electricalAnisotropy.getValue();

      sofa::defaulttype::Mat3x3d Df;
      Df = this->getFibreBase(tetraIndex);
      D = Df * D * Df.transposed();
   }

   double d11 = D[0][0];
   double d12 = D[0][1];
   double d13 = D[0][2];
   double d22 = D[1][1];
   double d23 = D[1][2];
   double d33 = D[2][2];

   detD = d11*(d22*d33 - d23*d23) - d12*(d12*d33 - d13*d23) + d13*(d12*d23 - d22*d13);
   P[0][0] = (d22*d33-d23*d23)/detD;
   P[0][1] = (d23*d13-d12*d33)/detD;
   P[0][2] = (d12*d23-d22*d13)/detD;
   P[1][0] = (d13*d23-d12*d33)/detD;
   P[1][1] = (d11*d33-d13*d13)/detD;
   P[1][2] = (d12*d13-d11*d23)/detD;
   P[2][0] = (d12*d23-d22*d13)/detD;
   P[2][1] = (d13*d12-d11*d23)/detD;
   P[2][2] = (d11*d22-d12*d12)/detD;

   w[0] = d_a*a[0] - d_b*b[0];
   w[1] = d_a*a[1] - d_b*b[1];
   w[2] = d_a*a[2] - d_b*b[2];

   A1 =
     (P[0][0]*a[0]*a[0] + P[1][1]*a[1]*a[1] + P[2][2]*a[2]*a[2])*d_a*d_a +
     (P[0][0]*b[0]*b[0] + P[1][1]*b[1]*b[1] + P[2][2]*b[2]*b[2])*d_b*d_b
     - 2.0*d_a*d_b*(P[0][0]*a[0]*b[0] + P[1][1]*a[1]*b[1] + P[2][2]*a[2]*b[2])
     + 2.0*d_a*d_a*(P[0][1]*a[0]*a[1] + P[0][2]*a[0]*a[2] + P[1][2]*a[1]*a[2])
     + 2.0*d_b*d_b*(P[0][1]*b[0]*b[1] + P[0][2]*b[0]*b[2] + P[1][2]*b[1]*b[2])
     - 2.0*d_a*d_b*(P[0][1]*a[0]*b[1] + P[0][2]*a[0]*b[2] + P[1][2]*a[1]*b[2])
     - 2.0*d_a*d_b*(P[0][1]*b[0]*a[1] + P[0][2]*b[0]*a[2] + P[1][2]*b[1]*a[2]);

   B1 =
     2.0*d_a*d_b*(P[0][0]*a[0]*b[0] + P[1][1]*a[1]*b[1] + P[2][2]*a[2]*b[2]) -
     2.0*d_b*d_b*(P[0][0]*b[0]*b[0] + P[1][1]*b[1]*b[1] + P[2][2]*b[2]*b[2]) +
     2.0*d_a*d_b*(P[0][1]*a[0]*b[1] + P[0][2]*a[0]*b[2] + P[1][2]*a[1]*b[2]) +
     2.0*d_a*d_b*(P[0][1]*b[0]*a[1] + P[0][2]*b[0]*a[2] + P[1][2]*b[1]*a[2]) -
     4.0*d_b*d_b*(P[0][1]*b[0]*b[1] + P[0][2]*b[0]*b[2] + P[1][2]*b[1]*b[2]);

   C1 =
     d_b*d_b*(P[0][0]*b[0]*b[0] + P[1][1]*b[1]*b[1] + P[2][2]*b[2]*b[2]) +
     2.0*d_b*d_b*(P[0][1]*b[0]*b[1] + P[0][2]*b[0]*b[2] + P[1][2]*b[1]*b[2]);

   temp1 = P[0][0]*w[0]*w[0] + P[1][1]*w[1]*w[1] + P[2][2]*w[2]*w[2] +
     2.0*P[0][1]*w[0]*w[1] + 2.0*P[0][2]*w[0]*w[2] + 2.0*P[1][2]*w[1]*w[2];

   temp2 = d_b*( P[0][0]*w[0]*b[0] + P[1][1]*w[1]*b[1] + P[2][2]*w[2]*b[2] + P[0][1]*w[0]*b[1] + P[0][1]*w[1]*b[0] + P[0][2]*w[0]*b[2] + P[0][2]*w[2]*b[0] + P[1][2]*w[1]*b[2] + P[1][2]*w[2]*b[1] );

   A2 = temp1*temp1;
   B2 = 2.0*temp1*temp2;
   C2 = temp2*temp2;

   temp1 = (Tb-Ta)*(Tb-Ta)*F*F;
   A1 *= temp1;
   B1 *= temp1;
   C1 *= temp1;

   flag_imag = this->find_roots_indic( q, 100.0*(A2-A1), 100.0*(B2-B1), 100.0*(C2-C1) );
   if(flag_imag) {
     q[0] = -1.0;
     q[1] = -1.0;
   }

   t = 0.0;
   vec[0] = b[0]*d_b;
   vec[1] = b[1]*d_b;
   vec[2] = b[2]*d_b;

   f_atZero = Tb + sqrt( P[0][0]*vec[0]*vec[0] + P[1][1]*vec[1]*vec[1] + P[2][2]*vec[2]*vec[2] + 2.0*P[0][1]*vec[0]*vec[1] + 2.0*P[0][2]*vec[0]*vec[2] + 2.0*P[1][2]*vec[1]*vec[2] )/F;

   t = 1.0;
   vec[0] = a[0]*d_a;
   vec[1] = a[1]*d_a;
   vec[2] = a[2]*d_a;

   f_atOne = Ta + sqrt( P[0][0]*vec[0]*vec[0] + P[1][1]*vec[1]*vec[1] + P[2][2]*vec[2]*vec[2] + 2.0*P[0][1]*vec[0]*vec[1] + 2.0*P[0][2]*vec[0]*vec[2] + 2.0*P[1][2]*vec[1]*vec[2] )/F;

   f = std::min(f_atOne, f_atZero);

   if(q[0] >= 0.0 && q[0] <= 1.0)  {
     t = q[0];
     vec[0] = a[0]*t*d_a + b[0]*(1.0-t)*d_b;
     vec[1] = a[1]*t*d_a + b[1]*(1.0-t)*d_b;
     vec[2] = a[2]*t*d_a + b[2]*(1.0-t)*d_b;

     f_q1 = Ta*t + (1.0-t)*Tb + sqrt( P[0][0]*vec[0]*vec[0] + P[1][1]*vec[1]*vec[1] + P[2][2]*vec[2]*vec[2] + 2.0*P[0][1]*vec[0]*vec[1] + 2.0*P[0][2]*vec[0]*vec[2] + 2.0*P[1][2]*vec[1]*vec[2] )/F;

     f = std::min(f, f_q1);
   }
   if(q[1] >= 0.0 && q[1] <= 1.0) {
     t = q[1];
     vec[0] = a[0]*t*d_a + b[0]*(1.0-t)*d_b;
     vec[1] = a[1]*t*d_a + b[1]*(1.0-t)*d_b;
     vec[2] = a[2]*t*d_a + b[2]*(1.0-t)*d_b;

     f_q2 = Ta*t + (1.0-t)*Tb + sqrt( P[0][0]*vec[0]*vec[0] + P[1][1]*vec[1]*vec[1] + P[2][2]*vec[2]*vec[2] + 2.0*P[0][1]*vec[0]*vec[1] + 2.0*P[0][2]*vec[0]*vec[2] + 2.0*P[1][2]*vec[1]*vec[2] )/F;

     f = std::min(f, f_q2);
   }

   return(f);
}


bool ZoneFMM::find_roots_indic(double *R, double a, double b, double c)
{
   double delta = b*b - 4.0*a*c;

   if(fabs(delta)<1e-10)
     delta = 0.0;

   if(delta < 0.0)
   {
     R[0] = 0.0;
     R[1] = 0.0;
     return(true);
   }
   else
   {
     R[0] = (-b + sqrt(delta))/(2.0*a);
     R[1] = (-b - sqrt(delta))/(2.0*a);
     return(false);
   }
}



void ZoneFMM::setHeartVertex(const unsigned int vertexIndex, const HeartVertex& vertexData)
{
   if (vertexIndex < m_dataVertices.getValue().size())
   {
      //helper::WriteAccessor <PointData <double> > my_dataVertices = m_depolarisationTime;
      sofa::helper::vector < HeartVertex >& my_dataVertices = *(m_dataVertices.beginEdit());
      my_dataVertices [vertexIndex] = vertexData;
      m_dataVertices.endEdit();
   }
   else
      std::cerr << "Error: In setDepolarizationTime, tetrahedron index is out of bound." << std::endl;

   return;
}


const HeartVertex ZoneFMM::getHeartVertex(const unsigned int vertexIndex)
{
   if (vertexIndex < m_dataVertices.getValue().size())
   {
      return m_dataVertices.getValue()[vertexIndex];
   }
   else
      std::cerr << "Error: In getDepolarizationTime, vertex index is out of bound. Returning default HeartVertex." << std::endl;

   return HeartVertex();
}


void ZoneFMM::setEikonalFlag (const unsigned int vertexIndex, const EikonalFlag flag)
{
   if (vertexIndex < m_flagContainer.getValue().size())
   {
      helper::vector <int>& my_flagContainer = *(m_flagContainer.beginEdit());
      my_flagContainer [vertexIndex] = (int)flag;
      m_flagContainer.endEdit();
   }
   else
      std::cerr << "Error: In setEikonalFlag, vertex index is out of bound." << std::endl;

   return;
}


ZoneFMM::EikonalFlag ZoneFMM::getEikonalFlag (const unsigned int vertexIndex)
{
   if (vertexIndex < m_flagContainer.getValue().size())
   {
      int flag =  m_flagContainer.getValue()[vertexIndex];
      return (ZoneFMM::EikonalFlag)flag;
   }
   else
      std::cerr << "Error: In getEikonalFlag, vertex index is out of bound. Returning TOOFAR flag" << std::endl;

   return (ZoneFMM::EikonalFlag)ZoneFMM::TOOFAR;
}


void ZoneFMM::setTetrahedronConductivity(unsigned int tetraIndex, double conductivity)
{
   if (tetraIndex < m_tetraConductivity.getValue().size())
   {
      sofa::helper::vector <double>& my_tetraConductivity = *(m_tetraConductivity.beginEdit());
      my_tetraConductivity [tetraIndex] = conductivity;
      m_tetraConductivity.endEdit();
   }
   else
      std::cerr << "Error: In setTetrahedronConductivity, tetra index is out of bound." << std::endl;

   return;
}


double ZoneFMM::getTetrahedronConductivity(const unsigned int tetraIndex)
{
   double conduc = 0.0;

   if (tetraIndex < m_tetraConductivity.getValue().size())
      conduc = m_tetraConductivity.getValue()[tetraIndex];
   else
      std::cerr << "Error: In getTetrahedronConductivity, tetra index is out of bound. Returning 0.0" << std::endl;

   return conduc;
}


void ZoneFMM::setTetrahedraConductivities(sofa::helper::vector<unsigned int>tetraIndices, sofa::helper::vector<double>conductivities)
{
   if (tetraIndices.size() != conductivities.size())
   {
      std::cerr << "Error: In setTetrahedraConductivities, input sizes differ." << std::endl;
      return;
   }

   sofa::helper::vector <double>& my_tetraConductivity = *(m_tetraConductivity.beginEdit());
   unsigned int nbr = tetraIndices.size();
   for (unsigned int i=0; i<nbr; ++i)
   {
      unsigned int index = tetraIndices[i];

      if (index < my_tetraConductivity.size())
         my_tetraConductivity[index] = conductivities[i];
      else
         std::cerr << "Error: In setTetrahedraConductivities, tetra index "<< index <<" is out of bound." << std::endl;

   }
   m_tetraConductivity.endEdit();

   return;
}


const sofa::helper::vector <double> ZoneFMM::getTetrahedraConductivities(sofa::helper::vector<unsigned int> tetraIndices)
{
   sofa::helper::vector <double> conductivities;

   unsigned int nbr = tetraIndices.size();
   conductivities.resize(nbr);
   for (unsigned int i=0; i<nbr; ++i)
   {
      unsigned int index = tetraIndices[i];

      if (index < m_tetraConductivity.getValue().size())
         conductivities[i] = m_tetraConductivity.getValue()[index];
      else
      {
         std::cerr << "Error: In getTetrahedraConductivities, tetra index "<< index <<" is out of bound. Adding 0.0 conductivity." << std::endl;
         conductivities[i] = 0.0;
      }
   }

   return conductivities;
}


void ZoneFMM::setNewWave(unsigned int vertexIndex, const HeartVertex &dataVertex)
{
   if (vertexIndex < m_dataVertices.getValue().size())
   {
      //helper::WriteAccessor <PointData <double> > my_dataVertices = m_depolarisationTime;
      sofa::helper::vector < HeartVertex >& my_dataVertices = *(m_dataVertices.beginEdit());
      (myData.FMaccepted).insert(std::pair<double, unsigned int> (my_dataVertices[vertexIndex].refractoryPeriod, vertexIndex));

      my_dataVertices [vertexIndex] = dataVertex;
      m_dataVertices.endEdit();

      helper::vector <int>& my_flagContainer = *(m_flagContainer.beginEdit());
      my_flagContainer[ vertexIndex ] = ZoneFMM::ACCEPTED;
      m_flagContainer.endEdit();

      this->updateNeighbours(vertexIndex);
   }
   else
      std::cerr << "Error: In setDepolarizationTime, tetrahedron index is out of bound." << std::endl;

   return;
}


sofa::defaulttype::Mat3x3d ZoneFMM::getFibreBase (const unsigned int tetraIndex)
{
   sofa::defaulttype::Mat3x3d base;
   if (tetraIndex < (unsigned int)_topology->getNbTetrahedra())
   {
      Vec3 f1 = this->getAnisotropicDirection(tetraIndex);
      //f1.normalize();
      Vec3 f2, f3;

      if (f1[2] != 1.0)
      {
         f2 = Vec3(-f1[1],f1[0],0.0);
         f2.normalize();
         f3 = Vec3(-f1[0]*f1[2],-f1[1]*f1[2],f1[0]*f1[0]+f1[1]*f1[1]);
         f3.normalize();
      }
      else
      {
         f2 = Vec3(1.0,0.0,0.0);
         f3 = Vec3(0.0,1.0,0.0);
      }

      for (unsigned int i=0; i<3; ++i)
      {
         base[i][0] = f1[i];
         base[i][1] = f2[i];
         base[i][2] = f3[i];
      }
   }
   else
   {
      std::cerr << "Error, index of tetrahedron is out of bound of fibers container. returning Identity matrix." << std::endl;
      base.identity();
   }

   return base;
}


sofa::defaulttype::Mat3x3d ZoneFMM::getLocalBase (const unsigned int tetraIndex)
{
   sofa::defaulttype::Vec<3,SReal> coords[4];
   sofa::defaulttype::Mat3x3d localBase; localBase.identity();
   Tetrahedron tetra = _topology->getTetrahedron(tetraIndex);

   for (unsigned int i=0; i<4; ++i)
      coords[i] = this->getPointPosition(tetra[i]);

   for (unsigned int i=0; i<3; ++i)
      for (unsigned int j=0; j<3; ++j)
         localBase[j][i] = coords[i+1][j] - coords[0][j];

   return localBase;
}


ZoneFMM::Vec3 ZoneFMM::getAnisotropicDirection(const unsigned int tetraIndex)
{
   Vec3 fibre; //fill with 0 by default

   if (p_useSyntheticFibers.getValue())
      return p_fiberDirection.getValue();

   if (tetraIndex < (m_tetraFibers.getValue()).size())
      fibre = (m_tetraFibers.getValue())[tetraIndex];
   else
      std::cerr << "Error, index of tetrahedron is out of bound of fibers container. returning null vector." << std::endl;

   return fibre;
}



ZoneFMM::Vec3 ZoneFMM::getLocalAnisotropicDirection(const unsigned int tetraIndex)
{
   Vec3 fibre;

   if (tetraIndex < (m_tetraBFibers.getValue()).size())
      fibre = (m_tetraBFibers.getValue())[tetraIndex];
   else
      std::cerr << "Error, index of tetrahedron is out of bound of fibers container in barycentric coordinates. returning null vector." << std::endl;

   return fibre;
}



void ZoneFMM::updateFibersDirection()
{
   helper::vector< Vec3 >& fibres = *((m_tetraFibers).beginEdit());
   const helper::vector< Vec3 >& fibresLocal = m_tetraBFibers.getValue();

   if (fibres.size() != fibresLocal.size())
      fibres.resize( fibresLocal.size());

   if (fibres.size() != (unsigned int)_topology->getNbTetrahedra())
   {
      std::cerr << "Error: Tetrahedron array and tetrahedra fibers don't share the same size." << std::endl;
      m_tetraFibers.endEdit();
      return;
   }

   for (unsigned int i =0; i<fibres.size(); i++)
   {
      sofa::defaulttype::Matrix3 transfo = this->getLocalBase(i);

      fibres[i] = transfo*fibresLocal[i];
      fibres[i].normalize();// = fibres[i]/fibres[i].norm();
   }

   m_tetraFibers.endEdit();
}



void ZoneFMM::exportDataFunction(ctime_t startTime)
{
   ctime_t endTime = CTime::getFastTime();
   ctime_t diffTime = endTime - startTime;

   unsigned int Acc = (myData.FMaccepted).size();
   unsigned int ChanAcc = (myData.FMchangedAccepted).size();
   unsigned int close = (myData.FMclose).size();
   unsigned int refrac = (myData.FMrefractory).size();

   if (outfile == NULL)
   {
      std::cerr << "Error: output steam pointer null. Can't export Data." << std::endl;
      std::cout << this->getContext()->getTime() << " , " << diffTime << ","<< Acc << "," << ChanAcc << "," << close << "," << refrac << std::endl;
      return;
   }

   std::cout << this->getContext()->getTime() << " , " << diffTime << ","<< Acc << "," << ChanAcc << "," << close << "," << refrac << std::endl;
   (*outfile) << this->getContext()->getTime() << " , " << diffTime << "," << Acc << "," << ChanAcc << "," << close << "," << refrac << "\n";
    outfile->flush();
}




void ZoneFMM::draw(const sofa::core::visual::VisualParams* /*vparams*/)
{
   if (_drawConduc.getValue())
   {
      /*
      glDisable(GL_LIGHTING);
      unsigned int nbr = _topocontainer->getNbTriangles();
      sofa::helper::vector<unsigned int> surfaceTri;
      for (unsigned int i=0; i<nbr; ++i)
      {
         if ( (_topocontainer->getTetrahedraAroundTriangle(i)).size() == 1)
            surfaceTri.push_back(i);
      }

      glColor3f(0.2, 0.2, 0.8);
      glBegin(GL_LINES);
      for (unsigned int i=0; i<surfaceTri.size(); ++i)
      {
         sofa::defaulttype::Vec<3,SReal> point[3];
         Triangle tri = _topocontainer->getTriangle(surfaceTri[i]);
         for (unsigned int j=0; j<3; ++j)
         {
            point[j] = _topoGeo->getPointPosition( tri[j]);
         }

         for (unsigned int j = 0; j<3; j++)
         {

            glVertex3d(point[j][0], point[j][1], point[j][2]);
            glVertex3d(point[(j+1)%3][0], point[(j+1)%3][1], point[(j+1)%3][2]);
         }
      }
      glEnd();



      std::multimap<double, unsigned int>::iterator itm;

      unsigned int nbrPointPol = myData.FMaccepted.size();
      sofa::helper::vector<unsigned int> vertexOnWave;
      vertexOnWave.resize(nbrPointPol);
      unsigned int cpt = 0;
      for (itm = (myData.FMaccepted).begin(); itm != (myData.FMaccepted).end(); ++itm)
      {
         vertexOnWave[cpt] = itm->second;
         cpt++;
      }

      sofa::helper::vector<unsigned int> triOnWave;

      // recupere tout les triangles de la vague:
      for (itm = (myData.FMaccepted).begin(); itm != (myData.FMaccepted).end(); ++itm)
      {
         sofa::helper::vector<unsigned int> triAV = _topocontainer->getTrianglesAroundVertex(itm->second);
         //unsigned int cptAV = 0;
         sofa::helper::vector<unsigned int> triGood;

         for (unsigned int i = 0; i<triAV.size(); ++i)
         {
            unsigned int triID = triAV[i];
            unsigned int cptTri = 0;
            Triangle the_tri = _topocontainer->getTriangle(triID);

            for ( unsigned int j = 0; j<3; ++j)
            {
               unsigned int vertexInTri = the_tri[j];
               for (unsigned int k = 0; k<vertexOnWave.size(); ++k)
                  if (vertexInTri == vertexOnWave[k])
                  {
                  cptTri++;
                  break;
               }
            }



            if (cptTri == 3)
               triGood.push_back(triID);

         }


         if (triGood.size() != triAV.size()) // on a du bord
         {
            for (unsigned int i = 0 ; i <triGood.size(); ++i)
               triOnWave.push_back(triGood[i]);
         }
      }


      glBegin(GL_TRIANGLES);
      glColor3f(0.8, 0.3, 0.3);


      for (unsigned int i = 0; i<triOnWave.size(); ++i)
      {

         glBegin(GL_TRIANGLES);
         glColor3f(1.0, 0.2, 0.2);

         unsigned int triID = triOnWave[i];
         Triangle the_tri = _topocontainer->getTriangle(triID);

         sofa::defaulttype::Vec<3,SReal> point[3];

         for (unsigned int j = 0; j<3; j++)
            point[j] = _topoGeo->getPointPosition(the_tri[j]);

         for (unsigned int j = 0; j<3; j++)
            glVertex3d(point[j][0], point[j][1], point[j][2]);

         glEnd();


         glBegin(GL_LINES);
         glColor3f(0.6, 0.6, 0.6);
         for (unsigned int j = 0; j<3; j++)
            glVertex3d(point[j][0], point[j][1], point[j][2]);

         glEnd();

      }

      glEnd();*/


      glDisable(GL_LIGHTING);
      unsigned int nbr = _topology->getNbTriangles();
      sofa::helper::vector<unsigned int> surfaceTri;
      for (unsigned int i=0; i<nbr; ++i)
      {
         if ( (_topology->getTetrahedraAroundTriangle(i)).size() == 1)
            surfaceTri.push_back(i);
      }

      glColor3f(0.7, 0.0, 0.2);
      glBegin(GL_LINES);
      for (unsigned int i=0; i<surfaceTri.size(); ++i)
      {
         sofa::defaulttype::Vec<3,SReal> point[3];
         Triangle tri = _topology->getTriangle(surfaceTri[i]);
         for (unsigned int j=0; j<3; ++j)
         {
            point[j] = this->getPointPosition( tri[j]);
         }

         for (unsigned int j = 0; j<3; j++)
         {

            glVertex3d(point[j][0], point[j][1], point[j][2]);
            glVertex3d(point[(j+1)%3][0], point[(j+1)%3][1], point[(j+1)%3][2]);
         }
      }
      glEnd();



      unsigned int nbrTetra = _topology->getNbTetrahedra();
      double conducNormal = this->m_electricalConductivity.getValue();
      const sofa::helper::vector <double>& tetraC = m_tetraConductivity.getValue();

      for (unsigned int i = 0; i<nbrTetra; ++i)
      {
         if (tetraC[i] == conducNormal)
            continue;

         double Ratio = tetraC[i]/conducNormal;
         glBegin(GL_TRIANGLES);
         glColor3f(0.7*Ratio, 0.0, 0.4*(1-Ratio));

         Tetrahedron tetra = _topology->getTetrahedron(i);
         sofa::defaulttype::Vec<3,SReal> point[4];

         for (unsigned int j = 0; j<4; j++)
            point[j] = this->getPointPosition(tetra[j]);

         for (unsigned int j = 0; j<4; j++)
         {
           glVertex3d(point[j][0], point[j][1], point[j][2]);
           glVertex3d(point[(j+1)%4][0], point[(j+1)%4][1], point[(j+1)%4][2]);
           glVertex3d(point[(j+2)%4][0], point[(j+2)%4][1], point[(j+2)%4][2]);
         }
         glEnd();

         glBegin(GL_LINES);
         glColor3f(0.5*Ratio, 0.0, 0.1*(1-Ratio));
         for (unsigned int j = 0; j<4; j++)
         {
           glVertex3d(point[j][0], point[j][1], point[j][2]);
           glVertex3d(point[(j+1)%4][0], point[(j+1)%4][1], point[(j+1)%4][2]);
           glVertex3d(point[(j+2)%4][0], point[(j+2)%4][1], point[(j+2)%4][2]);
         }
         glEnd();
      }

   }

   if (_drawFibers.getValue() && anisotropicFM)
   {
      unsigned int nbrTetra = _topology->getNbTetrahedra();

      Vec<3,SReal> point1, point2;
      Vec<3,double> colors;
      float fiberLength = _fiberLength.getValue();
      fiberLength *= 1/(m_electricalAnisotropy.getValue() + 0.1); // test

      glDisable(GL_LIGHTING);
      glBegin(GL_LINES);

      for (unsigned int i =0; i<nbrTetra; i++)
      {
         Tetrahedron tetrahedron = _topology->getTetrahedron(i);
         Vec3 fibre = this->getAnisotropicDirection(i);

         for(unsigned int j=0; j<3; j++)
            point1[j] = 0;

         for (unsigned int j = 0; j<4; j++)
            point1 += this->getPointPosition( tetrahedron[j] );

         point1 = point1/4;
         for(unsigned int j=0; j<3; j++)
            point2[j] = point1[j] + fibre[j]*fiberLength;

         for(unsigned int j=0; j<3; j++)
            colors[j] = fabs (fibre[j]);

         glColor3f (colors[0], colors[1], colors[2]);

         glVertex3d(point1[0], point1[1], point1[2]);
         glVertex3d(point2[0], point2[1], point2[2]);
      }
      glEnd();
   }
}


SOFA_DECL_CLASS(ZoneFMMimized)

int ZoneFMMClass = core::RegisterObject("Fast Marching algorithm for tetrahedral meshes.")
.add< ZoneFMM >();


} // namespace odesolver

} // namespace component

} // namespace sofa

