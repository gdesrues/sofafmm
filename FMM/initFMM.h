#ifndef INITFMM_H
#define INITFMM_H

#include "config.h"

#ifdef SOFA_BUILD_FMM
#define SOFA_FMM_API SOFA_EXPORT_DYNAMIC_LIBRARY
#else
#define SOFA_FMM_API SOFA_IMPORT_DYNAMIC_LIBRARY
#endif

/** mainpage
This is the main page of the doxygen documentation for MyPlugin.
 */

#endif
