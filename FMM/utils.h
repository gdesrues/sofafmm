#ifndef CARDIACREDUCTION_UTILS_H
#define CARDIACREDUCTION_UTILS_H

#include "config.h"
#include <sofa/core/visual/VisualParams.h>
#include <sofa/core/topology/TopologyElementHandler.h>

#define pass (void)0


namespace sofa
{


typedef SReal Real;
typedef helper::vector<Real> VReal;
typedef helper::vector<VReal> VVReal;

typedef unsigned int Int;
typedef helper::vector<Int> VInt;
typedef helper::vector<VInt> VVInt;
typedef helper::ReadAccessor<Data<VInt>> raVInt;
typedef helper::WriteOnlyAccessor<Data<VInt>> waVecInt;

typedef unsigned int Ref;
typedef helper::vector<Ref> VRef;
typedef helper::vector<VRef> VVRef;
typedef helper::ReadAccessor<Data<VRef>> raVRef;
typedef helper::WriteOnlyAccessor<Data<VRef>> waVRef;

typedef helper::vector<bool> VBool;
typedef helper::vector<std::string> VString;

static const Int spatial_dimensions = 3;
typedef defaulttype::Vec<spatial_dimensions,Real> Coord;
typedef helper::vector<Coord> VCoord;
typedef helper::WriteOnlyAccessor<Data<VCoord>> waPositions;
typedef helper::ReadAccessor<Data<VCoord>> raPositions;

typedef helper::WriteOnlyAccessor<Data<VVRef>> waZones;
typedef helper::ReadAccessor<Data<VVRef>> raZones;

typedef helper::ReadAccessor<Data<VVReal>> radistanceMap;
typedef helper::WriteOnlyAccessor<Data<VVReal>> wadistanceMap;

typedef sofa::core::topology::Topology::Tetrahedron Tetra;
typedef helper::vector<Tetra> VTetra;
typedef helper::vector<VTetra> VVTetra;
typedef helper::vector<VVTetra> VVVTetra;

typedef sofa::defaulttype::Quaternion Quaternion;
typedef sofa::defaulttype::Vector3 Vector3;
typedef sofa::defaulttype::Vec3f Vec3f;

typedef defaulttype::Mat<spatial_dimensions,spatial_dimensions,Real> Transform;
typedef helper::vector<Transform> VTransform;
typedef helper::WriteOnlyAccessor<Data<VTransform>> waTransform;
typedef helper::ReadAccessor<Data<VTransform>> raTransform;



//Coord getBarycenter(VCoord pts)
//{
//    Coord bary = {0,0,0};
//    size_t len = pts.size();
//    for(size_t i=0; i<len; ++i)
//        for(size_t j=0; j<3; ++j)
//            bary[j] += pts[i][j];
//    for(size_t j=0; j<3; ++j)
//        bary[j] /= (Real)len;
//    return bary;
//}


template<typename T>
bool isInRange(T e, helper::vector<T> ve)
{
    assert(ve.size()>0);
    T min(ve[0]), max(ve[0]);
    for (T ee : ve)
    {
        if(ee<min) min=ee;
        if(ee>max) max=ee;
    }
    return (min<=e and e<=max);
}

template<typename T1, typename T2, typename T3, typename T4>
void Print(T1 a, T2 b, T3 c, T4 d)
{
    std::cout << a << " " << b << " " << c << " " << d << std::endl;
}

template<typename T1, typename T2, typename T3>
void Print(T1 a, T2 b, T3 c)
{
    std::cout << a << " " << b << " " << c << std::endl;
}

template<typename T1, typename T2>
void Print(T1 a, T2 b)
{
    std::cout << a << " " << b << std::endl;
}

template<typename T>
void Print(T a)
{
    std::cout << a << std::endl;
}

template<typename T>
size_t rangMin(helper::vector<T> vec)
{
    assert(vec.size()>0);
    if(vec.size()==0) return 0;
    T dmin = vec[0]; size_t index=0;
    for(size_t i=1; i<vec.size(); ++i)
    {
        if (vec[i]<dmin)
        {
            dmin = vec[i];
            index = i;
        }
    }
    return index;
}

template<typename T>
T Min(helper::vector<T> vec)
{
    assert(vec.size()>0);
    if(vec.size()==0) return vec[0];
    T dmin = vec[0];
    for(size_t i=1; i<vec.size(); ++i)
        if (vec[i]<dmin)
            dmin = vec[i];
    return dmin;
}

template<typename T>
inline bool isIn(T a, helper::vector<T> list)
{
    for (T b : list) if(b==a) return true;
    return false;
}

template<typename T>
inline bool notIn(T a, helper::vector<T> list)
{
    for (T b : list) if(b==a) return false;
    return true;
}


} // namespace sofa

#endif
