# Fast Marching Algorithm
## Install
#### Sofa from sources
Clone/download this folder into the directory "external_plugins" of your sofa setup. You need to compile it and enable it in the cmake settings. Find some help in the [sofa documentation](https://www.sofa-framework.org/community/doc/using-sofa/build-a-plugin/).

#### Binaries of sofa
Note that you can compile this plugin with the binary version of sofa.
- Create a `build` folder and go into : `cd build`
- Launch cmake : `ccmake ../FMM`
- Configure : type `c`. If needed, press `t` to switch to advance mode
- You may need to add the path of the sofa libraries, for example :
 - `SofaBase_DIR` : `path_to_sofa_binaries/lib/cmake/SofaBase`
 - `SofaFramework_DIR` : `path_to_sofa_binaries/lib/cmake/SofaFramework`
- If needed : `Boost_INCLUDE_DIR` : `path_to_sofa_binaries/lib`

## Use
Go to the `Python` folder.

If you are familiar with Sofa, you can just run the scene with `runSofa scene_FMM_expert.py`. You can change the paths of the mesh and output within the scene.

If not, you need to install the python package `sofatools`.

```python
pip install sofatools
# or python3 -m pip install --user sofatools
```

Then open `launch.py`, here are the parameters you may change :

  | Key | Value |
  |:---:|:-----:|
  | `SOFA_PATH` | Path of runSofa : `path_to_sofa/bin/runSofa` |
  | `SCENE_PATH` | Path of the python scene |
  | `MESH` | Path to the vtk mesh |
  | `RESULT_DIR_PATH` | Directory for output and log, will be created if doesn't exist |
  | `INIT_NODES` | List of nodes for FMM initialisation (list or space-separated string) |
  | `EXPORT_AT_EACH_STEP` | If true, will export vtk files to see the propagation wave of the FMM |
  | `VTK_FILENAME` | Path for the vtk file(s) generated |

Finally, run
```python
python3 launch.py
```

## Output
![cubeFMM](Python/cube.png)
